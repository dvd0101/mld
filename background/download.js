// The windows that we open to start a download can be closed immediately;
// `disposable` maps the unique id that identifies the download (and that we
// include in the download url) with the a `Download` instance
const disposable = new Map();

const server = "http://192.168.11.15:4080";

class DisposableToken {
  constructor() {
    this._token = /&_mld=([^&]+)/;
  }
  add(url, value) {
    return `${url}&_mld=${window.encodeURIComponent(value)}`;
  }
  findToken(url) {
    const match = url.match(this._token);
    return match ? match[1] : null;
  }
}

const disposableToken = new DisposableToken();

class Download {
  constructor(link) {
    this.link = link;
    this.uniqueId = `_${Math.random()}`;
    this.windowId = null;
    this._resolve = null;
  }

  serverUrl() {
    const link = window.encodeURIComponent(this.link);
    const url = `${server}/submit?q=dllink+${link}`;
    return disposableToken.add(url, this.uniqueId);
  }

  openPopup(url) {
    const opt = {
      type: "panel",
      url,
    };
    browser.windows.create({type: "panel", height: 80, width: 400, url,})
      .then((win) => {
        this.windowId = win.id;
        log("window created", this.windowId);
        disposable.set(this.uniqueId, this);
      });
  }

  start() {
    return new Promise((resolve, reject) => {
      const url = this.serverUrl();
      log("start download of", url);
      this.openPopup(url);
      this._resolve = resolve;
    });
  }

  complete(feedback) {
    browser.windows.remove(this.windowId);
    this._resolve(feedback);
  }
}

function retrieveDownload(token) {
  const download = disposable.get(token);
  if (!download) {
    log("download not found!");
    return;
  }
  disposable.delete(token);
  return download;
}
