class FeedbackPage {
  constructor(port, token) {
    // this token identifies the popup window that will be closed once the
    // content script extracts the feedback data
    this.token = token;
    this.port = port;
    const handler = (msg) => {
      // feedback page must be closed asap
      log("<<<F", msg);
      this.port.onMessage.removeListener(handler);
      this.port.disconnect();

      const download = retrieveDownload(this.token);
      if (download) {
        download.complete(msg.feedback);
      }
    };
    this.port.onMessage.addListener(handler);
    this.port.postMessage({page_type: "feedback"});
  }
}

class ContentPage {
  constructor(port) {
    this.port = port;
    const handler = (msg) => {
      log("<<<C", msg);
      if (msg.download) {
        this.startDownload(msg.download);
      }
    };
    this.port.onMessage.addListener(handler);
    this.port.onDisconnect.addListener(() => {
      log("port disconnected");
      this.port.onMessage.removeListener(handler);
    });
    this.port.postMessage({page_type: "content"});
  }

  startDownload(link) {
    const d = new Download(link);
    d.start()
      .then((feedback) => {
        log("download complete", feedback);
        this.port.postMessage({link, feedback});
      });
  }
}
