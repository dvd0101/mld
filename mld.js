const ports = new Map();


function onPortConnected(port) {
  const pageUrl = port.name;
  const token = disposableToken.findToken(pageUrl);
  if (token) {
    // This port is open on a popup window used to start a download
    log("feedback page", pageUrl);
    new FeedbackPage(port, token);
  } else {
    log("content page", pageUrl);
    new ContentPage(port);
  }
}
browser.runtime.onConnect.addListener(onPortConnected);


function onPageMessage(message) {
  console.log(">>", message);
  if (message.download) {
    browser.notifications.create(
      "mld-download",
      {
        type: "basic",
        message: "starting a download",
        title: "MLD",
      });
    download(message.download);
  }
}

browser.runtime.onMessage.addListener(onPageMessage);
