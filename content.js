const protocols = {
  ed2k: {
    re: /^ed2k:\/\//,
    selector: "a[href^=ed2k\\:\\/\\/]",
  },
};

class FeedbackPage {
  constructor(port, change) {
    log("FeedbackPage");
    port.postMessage({feedback: this.extractFeedback()});
    change(undefined);
  }

  extractFeedback() {
    const header = document.querySelector(".results td.srh").innerText;
    if (header === "Unable to match URL") {
      const msg = document.querySelector(".results tr:nth-child(3) td.sr").innerText;
      if (msg.indexOf("File is already shared") !== -1) {
        return "ALREADY_DOWNLOADED";
      } else if(msg.indexOf("File is already in download queue") !== -1) {
        return "ALREADY_QUEUED";
      } else {
        log("unable to produce feedback", msg);
        return "UNKNOWN";
      }
    } else if (header === "Added link") {
      return "QUEUED";
    } else {
      log("unable to produce feedback", header);
      return "UNKNOWN";
    }
  }
}

class ContentPage {
  constructor(port, change) {
    log("ContentPage");
    const links = this.collectLinks();
    if (links.size === 0) {
      log("page without links");
      port.disconnect();
      return;
    }

    this.port = port;
    this.port.onMessage.addListener((msg) => {
      log("<<<CP", msg);
      if (msg.feedback) {
        this.applyFeedback(msg.link, msg.feedback);
      }
    });

    this.installClickHandler();
  }

  collectLinks() {
    const links = new Set();
    for (const p in protocols) {
      document.querySelectorAll(protocols[p].selector)
        .forEach((node) => { links.add(node.href); })
    }
    return links;
  }

  installClickHandler() {
    log("installClickHandler");
    document.addEventListener(
      "click",
      (evt) => {
        const href = evt.target.href;
        let submit = false;
        if (href && this.mustBeSubmitted(href)) {
          this.submit(href);
          evt.preventDefault();
        }
      },
      true);
  }

  mustBeSubmitted(href) {
    for (const p in protocols) {
      if (protocols[p].re.test(href)) {
        return true;
      }
    }
    return false;
  }

  submit(href) {
    log("submitting", href);
    this.port.postMessage({download: href});
  }

  applyFeedback(link, feedback) {
    document.querySelectorAll("a[href]")
      .forEach((node) => {
        if (node.href === link) {
          this.markNode(node, feedback);
        }
      });
  }

  markNode(node, feedback) {
    let msg;
    switch (feedback) {
      case "ALREADY_DOWNLOADED":
        msg = "already downloaded";
        break;
      case "ALREADY_QUEUED":
        msg = "already started";
        break;
      case "QUEUED":
        msg = "started";
        break;
      case "UNKNOWN":
      default:
        msg = "unknown";
        break;
    }

    const className = "_mld--feedback";
    node.querySelectorAll("." + className)
      .forEach((n) => node.removeChild(n));

    const el = document.createElement("span");
    el.className = className;
    el.innerText = `[${msg}]`;
    node.appendChild(el);
  }
}

class PageIdentification {
  constructor(url, change) {
    log("PageIdentification");
    this.port = browser.runtime.connect({name: url});

    const handler = (msg) => {
      this.port.onMessage.removeListener(handler);

      log("<<<PI", msg);
      switch (msg.page_type) {
        case "content":
          change(new ContentPage(this.port, change));
          break;
        case "feedback":
          change(new FeedbackPage(this.port, change));
          break;
      }
    };
    this.port.onMessage.addListener(handler);
  }
}

function start() {
  let receiver;
  const pageUrl = document.location.toString();
  receiver = new PageIdentification(pageUrl, (r) => { receiver = r; });
}

start();
